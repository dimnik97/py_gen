let length = 0

let data = {
  labels: [],
  datasets: [{
    label: "Prices",
    backgroundColor: "rgba(170, 234, 255, 0.5)",
    hoverBackgroundColor: "rgba(170, 234, 255,0.4)",
    data: [],
  }]
};

let options = {
  maintainAspectRatio: false,
  scales: {
    y: {
      stacked: true,
      grid: {
        display: true,
        color: "rgba(255,99,132,0.2)"
      }
    },
    x: {
      grid: {
        display: false
      }
    }
  }
};

let chart = new Chart("myChart", {
  type: "bar",
  options: options,
  data: data
});

const socket = io.connect('http://127.0.0.1:5005');

socket.on('message', data => {
    const { price } = JSON.parse(data)
    if (chart.data.datasets[0].data.length > 25) {
        chart.data.labels.shift();
        chart.data.datasets[0].data.shift();
    }
    chart.data.labels.push('');
    chart.data.datasets[0].data.push(price);
    chart.update();
});

function get_prices() {
    const ticker_item = document.getElementById("ticker_list").value
    socket.send(ticker_item)
}
