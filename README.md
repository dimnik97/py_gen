Real time prices generation

Done:
- relized a service that generate new prices every second.
- Service put data to kafka
- Consumer takes data from kafka and send it to html via socketio
- Data visualize with js lib for drawing graphs
- Kafka can be run via docker-compose up

TODO:
- Dockerize flask application properly
- Fix frontend issue  (when there are a lot of data in kafka and you press the button, graphs draws not smoothly)

How to run:
For now the app is dockerized patchy
To run kafka yu can exec: 

`docker-compose up -d`

When broker will be ready run:

`source start.sh`


Example:
![1](https://gitlab.com/dimnik97/py_gen/uploads/8ba790ad81e81a2259ef2f43da8c09af/FoERRDT_UzY.jpg)
