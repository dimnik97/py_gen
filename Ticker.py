import json
from random import random


class Ticker:
    def __init__(self, number):
        self.name = f"ticker_{number}"
        self.price = 0

    def __iter__(self):
        yield from {
            "name": self.name,
            "price": self.price
        }.items()

    def __str__(self):
        return json.dumps(dict(self), ensure_ascii=False)

    def __repr__(self):
        return self.__str__()

    def update_price(self):
        self.price += self.generate_movement()

    @staticmethod
    def generate_movement():
        movement = -1 if random() < 0.5 else 1
        return movement
