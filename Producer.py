import threading
import time
from kafka import KafkaProducer
from Ticker import Ticker


class Producer(threading.Thread):

    def __init__(self):
        self.producer = KafkaProducer(bootstrap_servers='localhost:9092')
        self.th = None

    def generate(self):
        ticker_list = []
        for i in range(100):
            element = Ticker(i)
            ticker_list.append(element)
            message = f"{element}"
            self.producer.send('prices', message.encode(encoding='UTF-8'))

        while True:
            time.sleep(1)
            for i in range(100):
                element = ticker_list[i]
                element.update_price()
                message = f"{element}"
                self.producer.send('prices', message.encode(encoding='UTF-8'))

    def start_generation(self):
        if not self.th:
            self.th = threading.Thread(target=self.generate, args=())
            self.th.start()
