import ast
from flask_socketio import SocketIO, send
from flask import Flask, render_template
from Consumer import Consumer
from Producer import Producer

app = Flask(__name__)
app.config['SECRET_KEY'] = 'very_secret_string'
socketio = SocketIO(app, cors_allowed_origins='*')
consumer_obj = None
producer = Producer()
producer.start_generation()


@app.route("/")
def start():
    ticker_list = []
    for i in range(100):
        ticker_list.append(f"ticker_{i}")
    return render_template('graph.html', ticker_list=ticker_list)


@socketio.on('message')
def get_prices_consumer(ticker_item):
    global consumer_obj
    if consumer_obj:
        consumer_obj.close()
    consumer = Consumer()
    consumer_obj = consumer.get_consumer()
    while True:
        for message in consumer_obj:
            data_str = message.value.decode("utf-8")
            data = ast.literal_eval(message.value.decode("utf-8"))
            if data["name"] == ticker_item:
                send(data_str, broadcast=True)


if __name__ == "__main__":
    socketio.run(app)
