from kafka import KafkaConsumer


class Consumer:

    def __init__(self):
        self.consumer = KafkaConsumer(bootstrap_servers='localhost:9092', auto_offset_reset='earliest', consumer_timeout_ms=1000)
        self.consumer.subscribe(['prices'])

    def get_consumer(self):
        return self.consumer

